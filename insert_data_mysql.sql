use tp2_devappweb_pokemon;

insert into regions values (null, 100, 100, 'NA', 'Region 1');
insert into regions values (null, 100, 100, 'NA', 'Region 2');
insert into regions values (null, 100, 100, 'NA', 'Region 3');
insert into regions values (null, 100, 100, 'NA', 'Region 4');
insert into regions values (null, 100, 100, 'NA', 'Region 5');
insert into regions values (null, 100, 100, 'NA', 'Region 6');
insert into regions values (null, 100, 100, 'NA', 'Region 7');

insert into types values (null, 'Type 1', 'NA');
insert into types values (null, 'Type 2', 'NA');
insert into types values (null, 'Type 3', 'NA');
insert into types values (null, 'Type 4', 'NA');
insert into types values (null, 'Type 5', 'NA');
insert into types values (null, 'Type 6', 'NA');

insert into pokemons values (null, 10, 100, 'NA', (select id from types where nom="Type 1"), (select id from regions where nom="Region 1"), '2021-01-01', "Description random", "Pokemon 1");
insert into pokemons values (null, 10, 100, 'NA', (select id from types where nom="Type 1"), (select id from regions where nom="Region 1"), '2021-01-01', "Description random", "Pokemon 2");
insert into pokemons values (null, 10, 100, 'NA', (select id from types where nom="Type 1"), (select id from regions where nom="Region 1"), '2021-01-01', "Description random", "Pokemon 3");
insert into pokemons values (null, 10, 100, 'NA', (select id from types where nom="Type 1"), (select id from regions where nom="Region 1"), '2021-01-01', "Description random", "Pokemon 4");
insert into pokemons values (null, 10, 100, 'NA', (select id from types where nom="Type 1"), (select id from regions where nom="Region 1"), '2021-01-01', "Description random", "Pokemon 5");
insert into pokemons values (null, 10, 100, 'NA', (select id from types where nom="Type 1"), (select id from regions where nom="Region 1"), '2021-01-01', "Description random", "Pokemon 6");
insert into pokemons values (null, 10, 100, 'NA', (select id from types where nom="Type 1"), (select id from regions where nom="Region 1"), '2021-01-01', "Description random", "Pokemon 7");
insert into pokemons values (null, 10, 100, 'NA', (select id from types where nom="Type 1"), (select id from regions where nom="Region 1"), '2021-01-01', "Description random", "Pokemon 8");
