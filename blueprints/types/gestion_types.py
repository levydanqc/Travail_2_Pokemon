

# pylint: disable=E0401

from flask import (
    Blueprint,
    abort,
    render_template,
    request
)
import bd
import os
from re import search

types_routes = Blueprint('types', __name__, url_prefix="/types",
                template_folder='templates')

@types_routes.route("/")
def typespage():
    """Fonction qui affiche la page de tous les types.

    Returns:
        Retourne la page des types.
    """
    connexion = bd.obtenir_connexion()
    curseur = connexion.cursor()
    curseur.execute("SELECT * FROM types")
    types = curseur.fetchall()
    curseur.close()
    connexion.close()
    return render_template("types.html", types=types)
    
@types_routes.route("/<int:monid>")
def type_informations(monid):
    """Permet d'afficher le type correspondant à l'id reçu en paramètres.

    Args:
        monid: L'id de la région.

    Returns:
        Retourne la page du type correspondant.
    """
    connexion = bd.obtenir_connexion()
    curseur = connexion.cursor()
    curseur.execute("SELECT * FROM types WHERE id=%s", (monid,))
    montype = curseur.fetchall()
    if not montype:
        abort(404)
    curseur.execute("SELECT * FROM pokemons WHERE type=%s", (monid,))
    pokemons = curseur.fetchall()
    curseur.close()
    connexion.close()
    return render_template("type_informations.html", type=montype, pokemons=pokemons)




@types_routes.route("/creation", methods=["POST", "GET"])
def type_creation():
    """Fonction qui affiche la page de creation de type.

    Returns:
        Retourne la page de creation de type.
    """
    if request.method == "POST":
        nom = request.form.get("nom").strip().capitalize()
        erreurs, file = type_verification(nom, request.files)
        if len(erreurs) > 0:
            return render_template("type_creation.html", erreurs=erreurs)
        file.seek(0, 0)
        file.save("static/files/" + file.filename)
        connexion = bd.obtenir_connexion()
        curseur = connexion.cursor()
        curseur.execute(
            "INSERT INTO types VALUES (null, %s, %s)", (nom, file.filename))
        connexion.commit()
        curseur.execute("SELECT * FROM types")
        types = curseur.fetchall()
        curseur.close()
        connexion.close()
        return render_template("types.html", types=types)
    return render_template("type_creation.html")



@types_routes.route("/<int:monid>/modification", methods=["POST", "GET"])
def type_modification(monid):
    """Fonction qui affiche la page de modification des types.

    Args:
        monid: L'id du type.

    Returns:
        Retourne la page pour modifier le type correspondant à monid.
    """
    connexion = bd.obtenir_connexion()
    curseur = connexion.cursor()
    curseur.execute("SELECT id FROM types WHERE id=%s", (monid,))
    if not curseur.fetchall():
        abort(404, "/types")
    if request.method == "POST":
        nom = request.form.get("nom").strip().capitalize()
        erreurs, file = type_verification(nom, request.files)
        if len(erreurs) > 0:
            return render_template("type_modification.html", erreurs=erreurs)
        file.seek(0, 0)
        file.save("static/files/" + file.filename)
        curseur.execute(
            "UPDATE types SET nom = %s, image = %s WHERE id = %s", (nom, file.filename, monid))
        connexion.commit()
        curseur.execute("SELECT * FROM types")
        types = curseur.fetchall()
        curseur.close()
        connexion.close()
        return render_template("types.html", types=types)
    return render_template("type_modification.html")


@types_routes.route("/<int:monid>/suppression", methods=["POST", "GET"])
def type_suppression(monid):
    """Fonction qui affiche la page pour supprimer un type.

    Args:
        monid: L'id du type.

    Returns:
        Retourne la page de suppression du type corresondant à monid.
    """
    connexion = bd.obtenir_connexion()
    curseur = connexion.cursor()
    curseur.execute("SELECT * FROM types WHERE id=%s", (monid,))
    montype = curseur.fetchall()
    curseur.execute("SELECT * FROM pokemons WHERE type=%s", (monid,))
    pokemons = curseur.fetchall()
    curseur.execute("SELECT id FROM types WHERE id=%s", (monid,))
    if not curseur.fetchall():
        abort(404, "/types")
    if request.method == "POST":
        confirmation = request.form.get("confirmation")
        deletepokemon = request.form.get("deletePokemons")

        if confirmation == "on":
            if not pokemons:
                curseur.execute("DELETE FROM types WHERE id=%s", (monid,))
                connexion.commit()
            else:
                erreurs = {}
                erreurs["population"] = "Ce type est encore attribué à un ou plusieurs pokemons."
                return render_template("type_suppression.html", type=montype, erreurs=erreurs)
        elif deletepokemon == "on":
            curseur.execute(
                "SELECT id FROM pokemons WHERE type=%s", (montype[0][0],))
            pokemonssupprimee = curseur.fetchall()
            for idpoke in pokemonssupprimee:
                curseur.execute(
                    "DELETE FROM batailles WHERE pokemon1=%s OR pokemon2=%s", (
                        idpoke[0], idpoke[0]))
            curseur.execute(
                "DELETE FROM pokemons WHERE type=%s", (montype[0][0],))
            curseur.execute("DELETE FROM types WHERE id=%s", (monid,))
            connexion.commit()
        else:
            erreurs = {}
            erreurs["confirmation"] = \
                "Vous devez cochez la case de confirmation pour supprimer une région."
            return render_template("type_suppression.html", type=montype, erreurs=erreurs)

        curseur.execute("SELECT * FROM types")
        types = curseur.fetchall()
        curseur.close()
        connexion.close()
        return render_template("types.html", types=types)
    curseur.close()
    connexion.close()
    return render_template("type_suppression.html", type=montype)


def type_verification(nom, files):
    """Fonction qui verifie les types.

    Args:
        nom: Le nom du type.
        files: Le dossier pour les images.

    Returns:
        Retourne les erreurs trouvées et les images.
    """
    erreurs = {}
    regex_valid = search(r'^[A-z-0-9éèëïàäâê\s-]+$', nom)
    if len(nom) > 45 or len(nom) == 0:
        erreurs["nom"] = "La longueur du nom doit être \
                entre 1 et 45 caractères."
    elif regex_valid is None or regex_valid.group(0) != nom:
        erreurs["nom"] = "Le nom doit respecter \
            les règles d'écriture."
    if 'image' not in files:
        erreurs["image"] = "Une image est requise."
    file = files['image']
    if file.filename == "":
        erreurs["image"] = "Une image est requise."
    if len(file.filename) > 45:
        erreurs["image"] = "Le nom de l'image est trop grand."
    file.seek(0, os.SEEK_END)
    if file.tell() > 1000 * 1000:
        erreurs["image"] = "La taille de l'image ne peux depasser 1 Mo."

    return erreurs, file