"""Cette application crée un site web de pokemons qui fonctionne avec Flask."""

from flask import Flask, render_template, request
from werkzeug.urls import url_encode, url_decode
import config
import bd
import hashlib
from blueprints.types.gestion_types import types_routes
from blueprints.regions.gestion_regions import regions_routes
from blueprints.pokemons.gestion_pokemons import pokemons_routes
from blueprints.comptes.gestion_comptes import comptes_routes

def creer_app():
    app_flask = Flask(__name__)
    app_flask.register_blueprint(types_routes)
    app_flask.register_blueprint(regions_routes)
    app_flask.register_blueprint(pokemons_routes)
    app_flask.register_blueprint(comptes_routes)
    app_flask.config.from_object(config.DevelopmentConfig)
    return app_flask


app = creer_app()
app.config["BABEL_DEFAULT_LOCALE"] = "fr_CA"

@app.route("/")
def accueil():
    """Fonction qui affiche toutes les régions sous forme d'images.

    Returns:
        Retourne l'index du site web.
    """
    connexion = bd.obtenir_connexion()
    curseur = connexion.cursor()
    curseur.execute("SELECT * FROM regions")
    regions = curseur.fetchall()
    curseur.close()
    connexion.close()
    return render_template("index.html", regions=regions)

def get_locale():
    """Fonction qui retourne la locale de Babel.

    Returns:
        Retourne la configuration locale de Babel.
    """
    return app.config["BABEL_DEFAULT_LOCALE"]

@app.route("/recherche")
def recherche():
    """Fonction qui retourne la page de recherche.

    Returns:
        Retourne la page de recherche.
    """
    return render_template("recherche.html")


@app.route("/search", methods=["GET", "POST"])
def search_request():
    """Fonction qui permet de rechercher un/des pokemons.

    Returns:
        Retourne la page qui contient le résultat de la recherche.
    """
    if request.method == "GET":
        pokemons = []
        query = url_encode({
            'q': request.args.get("search")
        })
        authorize_url = f'https://google.com/search?{query}'
        monsearch = url_decode(request.query_string)
        if not monsearch["search"].strip():
            erreurs = {}
            erreurs["longueur"] = "Les termes de la recherche ne peuvent être vide."
            return render_template("search.html", pokemons=pokemons,
                erreurs=erreurs, valeur=monsearch["search"], authorize_url=authorize_url)
        else :
            connexion = bd.obtenir_connexion()
            curseur = connexion.cursor()
            curseur.execute("SELECT * FROM pokemons WHERE nom like %s",
                        ("%" + monsearch["search"].strip().lower() + "%",))
            pokemons = curseur.fetchall()
        curseur.close()
        connexion.close()
        return render_template("search.html", pokemons=pokemons, valeur=monsearch["search"], authorize_url=authorize_url)
    return render_template("/search")


@app.errorhandler(404)
def page404(error):
    """La page d'erreur 404.

    Args:
        error: L'erreur.

    Returns:
        Retourne la page d'erreur 404.
    """
    return render_template("page404.html"), 404


@app.errorhandler(500)
def page500(error):
    """La page d'erreur 500.

    Args:
        error: L'erreur.

    Returns:
        Retourne la page d'erreur 500.
    """
    return render_template("page500.html"), 500


if __name__ == "__main__":
    app.run(debug=app.config["DEBUG"])
