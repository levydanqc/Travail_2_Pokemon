
# pylint: disable=E0401

from flask import (
    Blueprint,
    abort,
    render_template,
    request,
)
from re import search
from babel import numbers
import bd
import os

regions_routes = Blueprint('regions', __name__, url_prefix="/regions",
                template_folder='templates')
                

@regions_routes.route("/")
def region():
    """Fonction qui affiche l'ensemble des régions sous forme de cartes.

    Returns:
        Retourne la page qui affiche les régions.
    """
    connexion = bd.obtenir_connexion()
    curseur = connexion.cursor()
    curseur.execute("SELECT * FROM regions")
    regions = curseur.fetchall()
    curseur.close()
    connexion.close()
    return render_template("regions.html", regions=regions)


@regions_routes.route("/<int:monid>")
def region_informations(monid):
    """Fonction qui affiche une région.

    Args:
        monid: L'id de la région.

    Returns:
        Retourne la page qui affiche la région correspondante à monid.
    """
    infosinter = []
    connexion = bd.obtenir_connexion()
    curseur = connexion.cursor()
    curseur.execute("SELECT * FROM regions WHERE id=%s", (monid,))
    maregion = curseur.fetchall()
    if not maregion:
        abort(404)
    infosinter.append(

        #FIXME: CORRIGER LE GET LOCALE
        #def get_locale():
        #"""Fonction qui retourne la locale de Babel.

        #Returns:
        #    Retourne la configuration locale de Babel.
        #"""
        #return app.config["BABEL_DEFAULT_LOCALE"]

        numbers.format_decimal(maregion[0][1], locale="fr_CA"))
    infosinter.append(
        numbers.format_decimal(maregion[0][2], "fr_CA"))
    curseur.execute("SELECT * FROM pokemons WHERE region=%s", (monid,))
    pokemons = curseur.fetchall()
    curseur.close()
    connexion.close()
    return render_template("region_informations.html",
                           region=maregion, pokemons=pokemons, infosInter=infosinter)

@regions_routes.route("/creation", methods=["POST", "GET"])
def region_creation():
    """Fonction qui affiche le formulaire de création d'une région.

    Returns:
        Retourne la page de création d'une région.
    """
    if request.method == "POST":
        superficie = request.form.get("superficie").strip()
        population = request.form.get("population").strip()
        nom = request.form.get("nom").strip().capitalize()
        erreurs, file = region_verification(
            superficie, population, request.files, nom)
        if len(erreurs) > 0:
            return render_template("region_creation.html", erreurs=erreurs)
        file.seek(0, 0)
        file.save("static/files/" + file.filename)
        connexion = bd.obtenir_connexion()
        curseur = connexion.cursor()
        curseur.execute("INSERT INTO regions (superficie, population, image, nom) \
            VALUES (%s, %s, %s, %s)", (superficie, population, file.filename, nom))
        connexion.commit()
        curseur.execute("SELECT * FROM regions")
        regions = curseur.fetchall()
        curseur.close()
        connexion.close()
        return render_template("regions.html", regions=regions)
    return render_template("region_creation.html")

@regions_routes.route("/<int:monid>/modification", methods=["POST", "GET"])
def region_modification(monid):
    """Fonction qui affiche le formulaire de modification de la région.

    Args:
        monid: L'id de la région.

    Returns:
        Retourne la page de modification de la région correspondante à monid.
    """
    connexion = bd.obtenir_connexion()
    curseur = connexion.cursor()
    curseur.execute("SELECT id FROM regions WHERE id=%s", (monid,))
    if not curseur.fetchall():
        abort(404, "/regions")
    if request.method == "POST":
        superficie = request.form.get("superficie").strip()
        population = request.form.get("population").strip()
        nom = request.form.get("nom").strip().capitalize()
        erreurs, file = region_verification(
            superficie, population, request.files, nom)
        if len(erreurs) > 0:
            return render_template("region_modification.html", erreurs=erreurs)
        file.seek(0, 0)
        file.save("static/files/" + file.filename)
        connexion = bd.obtenir_connexion()
        curseur = connexion.cursor()
        curseur.execute("UPDATE regions SET superficie = %s, population = %s,\
             image = %s, nom = %s WHERE id = %s",
                        (superficie, population, file.filename, nom, monid))
        connexion.commit()
        curseur.execute("SELECT * FROM regions")
        regions = curseur.fetchall()
        curseur.close()
        connexion.close()
        return render_template("regions.html", regions=regions)
    return render_template("region_modification.html")

@regions_routes.route("/<int:monid>/suppression", methods=["POST", "GET"])
def region_suppression(monid):
    """Fonction qui affiche le formulaire de suppression de région.

    Args:
        monid: L'id de la région.

    Returns:
        valeur = request.args.get("search")
        Retourne la page de suppression de la région correspondante à monid.
    """
    connexion = bd.obtenir_connexion()
    curseur = connexion.cursor()
    curseur.execute("SELECT * FROM regions WHERE id=%s", (monid,))
    maregion = curseur.fetchall()
    curseur.execute("SELECT * FROM pokemons WHERE region=%s", (monid,))
    pokemons = curseur.fetchall()
    curseur.execute("SELECT id FROM regions WHERE id=%s", (monid,))
    if not curseur.fetchall():
        abort(404, "/regions")

    if request.method == "POST":
        confirmation = request.form.get("confirmation")
        deletepokemon = request.form.get("deletePokemons")

        if confirmation == "on":
            if not pokemons:
                curseur.execute("DELETE FROM regions WHERE id=%s", (monid,))
                connexion.commit()
            else:
                erreurs = {}
                erreurs["population"] = "La région contient encore des pokémons."
                return render_template("region_suppression.html", region=maregion, erreurs=erreurs)
        elif deletepokemon == "on":
            curseur.execute(
                "SELECT id FROM pokemons WHERE region=%s", (maregion[0][0],))
            pokemonsupprimee = curseur.fetchall()
            for idpoke in pokemonsupprimee:
                curseur.execute(
                    "DELETE FROM batailles WHERE pokemon1=%s OR pokemon2=%s", (
                        idpoke[0], idpoke[0]))
            curseur.execute(
                "DELETE FROM pokemons WHERE region=%s", (maregion[0][0],))
            curseur.execute("DELETE FROM regions WHERE id=%s", (monid,))
            connexion.commit()
        else:
            erreurs = {}
            erreurs["confirmation"] = \
                "Vous devez cochez la case de confirmation pour supprimer une région."
            return render_template("region_suppression.html", region=maregion, erreurs=erreurs)

        curseur.execute("SELECT * FROM regions")
        regions = curseur.fetchall()
        curseur.close()
        connexion.close()
        return render_template("regions.html", regions=regions)
    curseur.close()
    connexion.close()
    return render_template("region_suppression.html", region=maregion)



def region_verification(superficie, population, files, nom):
    """Fonction qui vérifie la region.

    Args:
        superficie: La superficie de la région.
        population: La population de la région.
        files: L'image de la région.
        nom: Le nom de la région.

    Returns:
        Retourne les erreurs et l'image.
    """
    erreurs = {}
    regex_valid = search(r'^[A-z-0-9éèëïàäâê\s-]+$', nom)
    if len(nom) > 45 or len(nom.strip()) == 0:
        erreurs["nom"] = "La longueur du nom doit être \
                entre 1 et 45 caractères."
    elif regex_valid is None or regex_valid.group(0) != nom:
        erreurs["nom"] = "Le nom doit respecter \
            les règles d'écriture."
    if not search(r'^[-+]{0,1}[0-9]+$', superficie) or int(superficie) < 0:
        erreurs["superficie"] = "La superficie \
            doit être un nombre entier supérieur ou égal à 0."
    if len(superficie) > 11 or len(superficie) <= 0:
        erreurs["superficie"] = "La superficie ne doit pas \
            contenir plus de 11 caractères et doit en contenir plus de 0."
    if not search(r'^[-+]{0,1}[0-9]+$', population) or int(population) < 0:
        erreurs["population"] = "La population \
            doit être un nombre entier supérieur ou égal à 0."
    if len(population) > 11 or len(population) <= 0:
        erreurs["population"] = "La population ne doit pas \
            dépasser 11 caractères et doit en contenir plus de 0."
    if 'image' not in files:
        erreurs["image"] = "Une image est requise."
    file = files['image']
    if file.filename == "":
        erreurs["image"] = "Une image est requise."
    if len(file.filename) > 45:
        erreurs["image"] = "Le nom de l'image est trop grand."
    file.seek(0, os.SEEK_END)
    if file.tell() > 1000 * 1000:
        erreurs["image"] = "La taille de l'image ne peux depasser 1 Mo."
    return erreurs, file