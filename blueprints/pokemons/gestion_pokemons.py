
# pylint: disable=E0401

from flask import (
    Blueprint,
    render_template,
    request,
    abort
)
import bd
from babel import (
    numbers,
    dates
)
import datetime
import os
from re import search


pokemons_routes = Blueprint('pokemons', __name__, url_prefix="/pokemons",
                template_folder='templates')

@pokemons_routes.route("/")
def pokemon():
    """Fonciton qui affiche tous les pokemons.

    Returns:
        Retourne la page d'affichage de tous les pokemons.
    """
    connexion = bd.obtenir_connexion()
    curseur = connexion.cursor()
    curseur.execute("SELECT * FROM regions")
    regions = curseur.fetchall()
    curseur.execute("SELECT * FROM types")
    types = curseur.fetchall()
    curseur.execute("SELECT * FROM pokemons ORDER BY nom ASC")
    pokemons = curseur.fetchall()
    curseur.close()
    connexion.close()
    return render_template("pokemons.html", pokemons=pokemons, regions=regions, types=types)


@pokemons_routes.route("/<int:monid>")
def pokemon_informations(monid):
    """Fonction qui affiche les informations d'un pokemon.

    Args:
        monid: L'id du pokemon.

    Returns:
        Retourne la page d'informations du pokemon correspondant à monid.
    """
    infosinter = []
    connexion = bd.obtenir_connexion()
    curseur = connexion.cursor()
    curseur.execute("SELECT * FROM pokemons WHERE id=%s", (monid,))
    monpokemon = curseur.fetchall()
    if not monpokemon:
        abort(404)
    #FIXME: CORRIGER LE GET LOCALE
        #def get_locale():
        #"""Fonction qui retourne la locale de Babel.

        #Returns:
        #    Retourne la configuration locale de Babel.
        #"""
        #return app.config["BABEL_DEFAULT_LOCALE"]
    infosinter.append(numbers.format_decimal(
        monpokemon[0][1], locale="fr_CA"))
    infosinter.append(numbers.format_decimal(
        monpokemon[0][2], locale="fr_CA"))
    infosinter.append(dates.format_date(monpokemon[0][6], locale="fr_CA"))
    curseur.execute("SELECT nom FROM regions WHERE id=%s", (monpokemon[0][5],))
    maregion = curseur.fetchall()
    curseur.execute("SELECT nom FROM types WHERE id=%s", (monpokemon[0][4],))
    montype = curseur.fetchall()
    curseur.close()
    connexion.close()
    return render_template("pokemon_informations.html",
                           pokemon=monpokemon, region=maregion, type=montype, infosInter=infosinter)





@pokemons_routes.route("/creation", methods=["POST", "GET"])
def pokemon_creation():
    """Fonction qui affiche le formulaire de création d'un pokemon.

    Returns:
        Retourne la page de création d'un pokemon.
    """
    connexion = bd.obtenir_connexion()
    curseur = connexion.cursor()
    curseur.execute("SELECT * FROM regions")
    regionsid = curseur.fetchall()
    curseur.execute("SELECT * FROM types")
    types = curseur.fetchall()
    if request.method == "POST":
        nom = request.form.get("nom").strip().capitalize()
        date_creation = datetime.date.today()
        taille = request.form.get("taille").strip()
        poids = request.form.get("poids").strip()
        letype = request.form.get("type")
        regionid = request.form.get("regions")
        description = request.form.get("description").strip()
        erreurs, file = pokemon_verification(
            nom, taille, poids, request.files, description)
        if len(erreurs) > 0:
            return render_template("pokemon_creation.html",
                                   erreurs=erreurs, regionsID=regionsid, types=types)
        file.seek(0, 0)
        file.save("static/files/" + file.filename)
        curseur.execute("INSERT INTO pokemons \
            (taille, poids, image, type, region, date_creation, description, nom) \
                VALUES (%s,%s,%s,%s,%s,%s,%s,%s)", (taille, poids, file.filename, letype,
                                                    regionid, date_creation, description, nom))
        connexion.commit()
        curseur.execute("SELECT * FROM pokemons ORDER BY nom ASC")
        pokemons = curseur.fetchall()
        curseur.execute("SELECT * FROM regions")
        regions = curseur.fetchall()
        curseur.close()
        connexion.close()
        return render_template("pokemons.html", pokemons=pokemons, regions=regions, types=types)
    return render_template("pokemon_creation.html", regionsID=regionsid, types=types)


@pokemons_routes.route("/<int:monid>/modification", methods=["POST", "GET"])
def pokemon_modification(monid):
    """Fonciton qui affiche le formulaire de modification du pokemon.

    Args:
        monid: L'id du pokemon.

    Returns:
        Retourne la page de modification du pokemon.
    """
    connexion = bd.obtenir_connexion()
    curseur = connexion.cursor()
    curseur.execute("SELECT * FROM regions")
    regionsid = curseur.fetchall()
    curseur.execute("SELECT * FROM types")
    types = curseur.fetchall()
    curseur.execute("SELECT id FROM pokemons WHERE id=%s", (monid,))
    if not curseur.fetchall():
        abort(404, "/pokemons")

    if request.method == "POST":
        nom = request.form.get("nom").strip().capitalize()
        taille = request.form.get("taille").strip()
        poids = request.form.get("poids").strip()
        letype = request.form.get("type")
        regionid = request.form.get("regions")
        description = request.form.get("description").strip()
        erreurs, file = pokemon_verification(nom, taille, poids,
                                             request.files, description)
        if len(erreurs) > 0:
            return render_template("pokemon_modification.html",
                                   erreurs=erreurs, regionsID=regionsid, types=types)
        file.seek(0, 0)
        file.save("static/files/" + file.filename)
        curseur.execute("UPDATE pokemons SET taille=%s, poids=%s, image=%s, \
            type=%s, region=%s, description=%s, nom=%s WHERE id = %s",
                        (taille, poids, file.filename, letype, regionid, description, nom, monid))
        connexion.commit()
        curseur.execute("SELECT * FROM pokemons ORDER BY nom ASC")
        pokemons = curseur.fetchall()
        curseur.execute("SELECT * FROM regions")
        regions = curseur.fetchall()
        curseur.close()
        connexion.close()
        return render_template("pokemons.html", pokemons=pokemons, regions=regions, types=types)
    return render_template("pokemon_modification.html", regionsID=regionsid, types=types)


@pokemons_routes.route("/<int:monid>/suppression", methods=["POST", "GET"])
def pokemon_suppression(monid):
    """Fonction qui affiche le formulaire de suppression du pokemon.

    Args:
        monid: L'id du pokemon.

    Returns:
        Retourne la page de suppression du pokemon.
    """
    connexion = bd.obtenir_connexion()
    curseur = connexion.cursor()
    curseur.execute("SELECT * FROM pokemons WHERE id=%s", (monid,))
    monpokemon = curseur.fetchall()
    curseur.execute("SELECT id FROM pokemons WHERE id=%s", (monid,))
    if not curseur.fetchall():
        abort(404, "/pokemons")
    if request.method == "POST":
        confirmation = request.form.get("confirmation")
        if confirmation == "on":
            curseur.execute(
                "DELETE FROM batailles WHERE pokemon1=%s OR pokemon2=%s", (monid, monid))
            connexion.commit()
            curseur.execute("DELETE FROM pokemons WHERE id=%s", (monid,))
            connexion.commit()
        curseur.execute("SELECT * FROM regions")
        regions = curseur.fetchall()
        curseur.execute("SELECT * FROM types")
        types = curseur.fetchall()
        curseur.execute("SELECT * FROM pokemons ORDER BY nom ASC")
        pokemons = curseur.fetchall()
        curseur.close()
        connexion.close()
        return render_template("pokemons.html", pokemons=pokemons, regions=regions, types=types)
    curseur.close()
    connexion.close()
    return render_template("pokemon_suppression.html", pokemon=monpokemon)

@pokemons_routes.route("/batailles", methods=["POST", "GET"])
def batailles():
    """Fonction qui effectue et affiche les batailles.

    Returns:
        Retourne la page des batailles.
    """
    pokemonscombats = []
    connexion = bd.obtenir_connexion()
    curseur = connexion.cursor()
    curseur.execute("SELECT * FROM pokemons")
    pokemons = curseur.fetchall()
    curseur.execute("SELECT * FROM batailles ORDER BY id DESC LIMIT 10")
    lesbatailles = curseur.fetchall()
    if len(lesbatailles) != 0:
        curseur.execute("SELECT c.nom, c.poids, c.id, e.nom, \
            e.poids, e.id, d.date FROM pokemons c \
            INNER JOIN batailles d ON c.id = d.pokemon1 INNER JOIN pokemons e ON d.pokemon2 \
                = e.id ORDER BY d.id DESC LIMIT 10")
        combats = curseur.fetchall()
        for combat in combats:
            #FIXME: GETLOCALE
            pokemonscombats.append((combat[0], combat[1], combat[2], combat[3], combat[4],
                                    combat[5], dates.format_date((combat[6]), locale="fr_CA")))
    if request.method == "POST":
        pokemonscombats = []
        erreurs = {}
        date_creation = datetime.date.today()
        combattantgauche = request.form.get("combatGauche")
        combattantdroite = request.form.get("combatDroite")
        if combattantgauche == "" or combattantdroite == "":
            erreurs["matchmaking"] = "Veuillez sélectionner un pokémon."
            return render_template("bataille.html", erreurs=erreurs,
                                   pokemonscombats=pokemonscombats, pokemons=pokemons)
        if combattantgauche != "" and combattantdroite != "" and int(combattantgauche)\
                == int(combattantdroite):
            erreurs["matchmaking"] = "Les pokemons ne peuvent être les mêmes."
            if len(lesbatailles) != 0:
                return render_template("bataille.html", erreurs=erreurs,
                                       pokemonscombats=pokemonscombats, pokemons=pokemons)
            return render_template("bataille.html", erreurs=erreurs, pokemons=pokemons)
        curseur.execute("INSERT INTO batailles (pokemon1, pokemon2, date) VALUES (%s,%s,%s)",
                        (combattantgauche, combattantdroite, date_creation))
        connexion.commit()
        curseur.execute("SELECT c.nom, c.poids, c.id, e.nom, e.poids, e.id, d.date FROM pokemons c \
            INNER JOIN batailles d ON c.id = d.pokemon1 INNER JOIN pokemons e ON d.pokemon2 \
                = e.id ORDER BY d.id DESC LIMIT 10")
        combats = curseur.fetchall()
        for combat in combats:
            #FIXME: GETLOCALE
            pokemonscombats.append(
                (combat[0], combat[1], combat[2], combat[3], combat[4], combat[5],
                                    dates.format_date((combat[6]), locale="fr_CA")))
        curseur.close()
        connexion.close()
        return render_template("bataille.html", pokemonscombats=pokemonscombats, pokemons=pokemons)
    if len(lesbatailles) != 0:
        return render_template("bataille.html", pokemons=pokemons,
                               pokemonscombats=pokemonscombats)
    return render_template("bataille.html", pokemons=pokemons)

def pokemon_verification(nom, taille, poids, files, description):
    """Fonction qui vérifie le pokemon.

    Args:
        nom: Le nom du pokemon.
        taille: La taille du pokemon.
        poids: Le poids du pokemon.
        files: Le dossier contenant les images.
        description: La description du pokemon.

    Returns:
        Retourne les erreurs et le dossier files.
    """
    erreurs = {}
    regex_valid = search(r'^[A-z-0-9éèëïàäâê\s-]+$', nom)
    if len(nom) > 45 or len(nom.strip()) == 0:
        erreurs["nom"] = "La longueur du nom doit être \
                entre 1 et 45 caractères."
    elif regex_valid is None or regex_valid.group(0) != nom:
        erreurs["nom"] = "Le nom doit respecter \
                les règles d'écriture."
    if not search(r'^[-+]{0,1}[0-9]+$', taille) or int(taille) < 0:
        erreurs["taille"] = "La taille \
                doit être un nombre entier supérieure ou égale à 0."
    if not search(r'^[-+]{0,1}[0-9]+$', poids) or int(poids) < 0:
        erreurs["poids"] = "Le poids \
            doit être un nombre entier et supérieure ou égale à 0."
    if len(description.strip()) == 0:
        erreurs["description"] = "La longueur de la description doit être \
                de 1 caractère ou plus."
    if 'image' not in files:
        erreurs["image"] = "Une image est requise."
    file = files['image']
    if file.filename == "":
        erreurs["image"] = "Une image est requise."
    if len(file.filename) > 45:
        erreurs["image"] = "Le nom de l'image est trop grand."
    file.seek(0, os.SEEK_END)
    if file.tell() > 1000 * 1000:
        erreurs["image"] = "La taille de l'image ne peux depasser 1 Mo."
    return erreurs, file
