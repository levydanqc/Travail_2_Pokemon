from flask import(
    Blueprint,
    session,
    url_for,
    request,
    redirect
)

comptes_routes = Blueprint('comptes', __name__, url_prefix="/comptes",
                template_folder='templates')

@comptes_routes.route("/")
def index():
    if 'username' in session['username']:
        return 'Connecté en tant que ' + username
    return "Vous n'êtes pas connecté. <br><a href = '/login'>" + "click here to log in</a>"

@comptes_routes.route("/authentification", methods = ['GET', 'POST'])
def authentification():
    if request.method == 'POST':
        session['username'] = request.form['username']
        return redirect(url_for('index'))
    return ""

@comptes_routes.route('deconnexion')
def deconnexion():
    session.pop('username', None)
    return redirect(url_for('index'))